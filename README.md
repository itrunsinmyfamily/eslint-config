# @ itruns/eslint-config

> ItRunsInMyFamily [ESLint](http://eslint.org) [shareable configurations](http://eslint.org/docs/developer-guide/shareable-configs).

## Installation

```sh
$ npm install --save-dev git+ssh://git@bitbucket.org/itrunsinmyfamily/eslint-config.git
```

## Usage

Add **one** of the following lines to your project's `.eslintrc`:

```js
{
	"extends": "@itruns",
	"extends": "@itruns/eslint-config/mocha"
}
```

## License

MIT © ItRunsInMyFamily