module.exports = {
  'root': true,
  'extends': ['airbnb-base', 'plugin:node/recommended'],
  'plugins': [
    'promise',
    'node',
    'filenames',
    'no-use-extend-native',
  ],
  'env': {
    'browser': false,
    'es6': true,
    'node': true
  },
  'rules': {
    'max-len': ['warn', 120],

    'id-match': [
      'error',
      // identifiers must always be camel cased, and never have two
      // or more contiguous upper case characters.
      // Alternatively they can be all uppercase.
      '^([A-Z_]{2,}|[A-Z]?[a-z]+([A-Z]{1,2}[a-z]+)*[A-Z]?|id|JSON|_|_.*)$',
      {
        // identifiers in properties are also checked
        'properties': false,
      }
    ],
    // Disallow use of inline numbers
    'no-magic-numbers': ['warn', { ignore: [-1,0,1] }],

    // allow/disallow an empty newline after var statement
    'newline-after-var': [
      'error',
      'always',
    ],

    // require parens in arrow function arguments only when needed
    'arrow-parens': ['error', 'as-needed'],

    'no-inline-comments': 'warn',

    'no-prototype-builtins': 'off',

    'no-use-before-define': ['error', { functions: false, classes: false }],

    'new-cap': ['error', { 'properties': false }],

    // Require no semi colon.
    'semi': ['error', 'never'],

    // Protect against code that produces problems when semicolons are absent
    'no-unexpected-multiline': 'error',

    // Enforce handling of callback error handles
    'handle-callback-err': 'error',

    // Allow only console.warn and console.error
    'no-console': ['warn', { 'allow': ['warn', 'error'] }],

    // Enforce correct linebreaks
    'linebreak-style': 'error',

    // Promise plugin: Enforce good behavior with promises
    'promise/param-names': 'error',
    'promise/always-return': 'error',
    'promise/catch-or-return': 'error',

    // Node plugin: Enforce feature set of our node version (v6)
    'node/no-unsupported-features': ['error', { 'version': 6 }],
    'node/no-unpublished-require': 'off',

    // Filenames plugin: Enforcing kebabCase filenames
    'filenames/match-regex': ['error', '^[a-z.]+(?:-[a-z.]+)*$', true],

    'no-underscore-dangle': ['error', { 'allowAfterThis': true }]
  }
}
