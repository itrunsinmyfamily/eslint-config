module.exports = {
  'extends': [ '@itruns' ],
  'env': {
    'mocha': true,
  },
  'plugins': [
    'mocha'
  ],
  'rules': {
    'prefer-arrow-callback': 'off',
    'func-names': 'off',
    'promise/always-return': 'off',
    'no-magic-numbers': 'off',
    'no-param-reassign': ['error', { props: false }],

    'import/no-extraneous-dependencies': ['error', { devDependencies: true }],

    // Mocha plugin: Enforcing good behavior in tests
    'mocha/no-exclusive-tests': 'error',
    'mocha/no-skipped-tests': 'error',
    'mocha/no-pending-tests': 'error',
    'mocha/handle-done-callback': 'error',
    'mocha/no-synchronous-tests': 'error',
    'mocha/no-global-tests': 'error',

  }
}
